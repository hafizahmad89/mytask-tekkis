<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $key_id
 * @property string $reff_no
 * @property string $invoice_no
 * @property string $amount
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $desc
 * @property string $created_at
 * @property string $updated_at
 */
class Payment extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['key_id', 'reff_no', 'invoice_no', 'amount', 'name', 'email', 'phone', 'desc', 'created_at', 'updated_at'];
}
