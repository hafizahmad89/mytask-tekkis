<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Payment;
use Illuminate\Support\Facades\Http;

class PaymentController extends Controller
{
    /**
     * Handle an incoming payment request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function payprocess(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:255'],
            'desc' => ['required', 'string'],
            'amount' => ['required', 'string'],
        ]);

        $dataPay = json_encode([
            "merchantKey" => "DGlVOMk6EXpOCGIDgzF53XP80Qr-qWSbbjI4YhJfFm6sgZRRzn3GdIb8WOUWA9hJeeRJaoK09lxsPd1NtQfj5bwYg2EyZlfgLPRC",
            "signature" => "8a8b75360d5040a9715b612a4cc5f27139b197ae457d3ac80ca3d20c488fec51",
            "paymentName" => $request->name,
            "paymentEmail" => $request->email,
            "paymentPhone" => $request->phone,
            "paymentDesc" => $request->desc,
            "paymentAmount" => $request->amount,
            "paymentStatus" => "pending",
            "paymentType" => "Ewallet,FPX,Card",
            "paymentRefNo" => "INV-000001",
            "paymentCallbackURL" => "https://webhook.site/820bf3bf-75ba-4182-a289-a01d81c9180c",
            "paymentRedirectURL" => "www.facebook.com"
        ]);

        $payload = base64_encode($dataPay);

        $response = Http::post('https://api.tpay.com.my/payment/GenerateTpaymentToken', [
            'payload' => $payload
        ]);

        $dataSuccess = json_decode($response, true);
        //print_r($dataSuccess);
        $urlLink = $dataSuccess['response']['paymentDetails']['payment_link'];
       
        if($dataSuccess){
            $user = Payment::create([
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone,
                'desc' => $request->desc,
                'amount' => $request->amount,
                'key_id' => $dataSuccess['response']['paymentDetails']['payment_key_id'],
                'reff_no' => $dataSuccess['response']['paymentDetails']['payment_ref_no'],
                'invoice_no' => $dataSuccess['response']['paymentDetails']['payment_invoice_no'],
            ]);

            return redirect($urlLink);
        }
    }
}
