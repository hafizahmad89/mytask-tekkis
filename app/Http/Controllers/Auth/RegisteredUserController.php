<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:'.User::class],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        $refid = $request->ref_id;
        if($refid){
            $userValid = User::where('user_id', '=', $refid)->count();
            if($userValid>0){
                $userWallet = User::where('user_id', $refid)->first();
                $userRef = $userWallet->user_id;
                
                $userWallet->wallet = $userWallet->wallet + 25;
                $userWallet->save();
            }
        }else{
            $userRef = "";
        }

        $userNo = User::count();
        $userNo = $userNo + 1;
        $nameCode="REF";
        $userCode=sprintf($nameCode."%05d",$userNo++);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'user_id' => $userCode,
            'reff_id' => $userRef,
            'wallet' => 0,
        ]);

        event(new Registered($user));

        Auth::login($user);

        return redirect(RouteServiceProvider::HOME);
    }
}
