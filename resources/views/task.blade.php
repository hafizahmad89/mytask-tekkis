<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */html{line-height:1.15;-webkit-text-size-adjust:100%}body{margin:0}a{background-color:transparent}[hidden]{display:none}html{font-family:system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;line-height:1.5}*,:after,:before{box-sizing:border-box;border:0 solid #e2e8f0}a{color:inherit;text-decoration:inherit}svg,video{display:block;vertical-align:middle}video{max-width:100%;height:auto}.bg-white{--tw-bg-opacity: 1;background-color:rgb(255 255 255 / var(--tw-bg-opacity))}.bg-gray-100{--tw-bg-opacity: 1;background-color:rgb(243 244 246 / var(--tw-bg-opacity))}.border-gray-200{--tw-border-opacity: 1;border-color:rgb(229 231 235 / var(--tw-border-opacity))}.border-t{border-top-width:1px}.flex{display:flex}.grid{display:grid}.hidden{display:none}.items-center{align-items:center}.justify-center{justify-content:center}.font-semibold{font-weight:600}.h-5{height:1.25rem}.h-8{height:2rem}.h-16{height:4rem}.text-sm{font-size:.875rem}.text-lg{font-size:1.125rem}.leading-7{line-height:1.75rem}.mx-auto{margin-left:auto;margin-right:auto}.ml-1{margin-left:.25rem}.mt-2{margin-top:.5rem}.mr-2{margin-right:.5rem}.ml-2{margin-left:.5rem}.mt-4{margin-top:1rem}.ml-4{margin-left:1rem}.mt-8{margin-top:2rem}.ml-12{margin-left:3rem}.-mt-px{margin-top:-1px}.max-w-6xl{max-width:72rem}.min-h-screen{min-height:100vh}.overflow-hidden{overflow:hidden}.p-6{padding:1.5rem}.py-4{padding-top:1rem;padding-bottom:1rem}.px-6{padding-left:1.5rem;padding-right:1.5rem}.pt-8{padding-top:2rem}.fixed{position:fixed}.relative{position:relative}.top-0{top:0}.right-0{right:0}.shadow{--tw-shadow: 0 1px 3px 0 rgb(0 0 0 / .1), 0 1px 2px -1px rgb(0 0 0 / .1);--tw-shadow-colored: 0 1px 3px 0 var(--tw-shadow-color), 0 1px 2px -1px var(--tw-shadow-color);box-shadow:var(--tw-ring-offset-shadow, 0 0 #0000),var(--tw-ring-shadow, 0 0 #0000),var(--tw-shadow)}.text-center{text-align:center}.text-gray-200{--tw-text-opacity: 1;color:rgb(229 231 235 / var(--tw-text-opacity))}.text-gray-300{--tw-text-opacity: 1;color:rgb(209 213 219 / var(--tw-text-opacity))}.text-gray-400{--tw-text-opacity: 1;color:rgb(156 163 175 / var(--tw-text-opacity))}.text-gray-500{--tw-text-opacity: 1;color:rgb(107 114 128 / var(--tw-text-opacity))}.text-gray-600{--tw-text-opacity: 1;color:rgb(75 85 99 / var(--tw-text-opacity))}.text-gray-700{--tw-text-opacity: 1;color:rgb(55 65 81 / var(--tw-text-opacity))}.text-gray-900{--tw-text-opacity: 1;color:rgb(17 24 39 / var(--tw-text-opacity))}.underline{text-decoration:underline}.antialiased{-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.w-5{width:1.25rem}.w-8{width:2rem}.w-auto{width:auto}.grid-cols-1{grid-template-columns:repeat(1,minmax(0,1fr))}@media (min-width:640px){.sm\:rounded-lg{border-radius:.5rem}.sm\:block{display:block}.sm\:items-center{align-items:center}.sm\:justify-start{justify-content:flex-start}.sm\:justify-between{justify-content:space-between}.sm\:h-20{height:5rem}.sm\:ml-0{margin-left:0}.sm\:px-6{padding-left:1.5rem;padding-right:1.5rem}.sm\:pt-0{padding-top:0}.sm\:text-left{text-align:left}.sm\:text-right{text-align:right}}@media (min-width:768px){.md\:border-t-0{border-top-width:0}.md\:border-l{border-left-width:1px}.md\:grid-cols-2{grid-template-columns:repeat(2,minmax(0,1fr))}}@media (min-width:1024px){.lg\:px-8{padding-left:2rem;padding-right:2rem}}@media (prefers-color-scheme:dark){.dark\:bg-gray-800{--tw-bg-opacity: 1;background-color:rgb(31 41 55 / var(--tw-bg-opacity))}.dark\:bg-gray-900{--tw-bg-opacity: 1;background-color:rgb(17 24 39 / var(--tw-bg-opacity))}.dark\:border-gray-700{--tw-border-opacity: 1;border-color:rgb(55 65 81 / var(--tw-border-opacity))}.dark\:text-white{--tw-text-opacity: 1;color:rgb(255 255 255 / var(--tw-text-opacity))}.dark\:text-gray-400{--tw-text-opacity: 1;color:rgb(156 163 175 / var(--tw-text-opacity))}.dark\:text-gray-500{--tw-text-opacity: 1;color:rgb(107 114 128 / var(--tw-text-opacity))}}
        </style>

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body class="antialiased">
        <div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
            @if (Route::has('login'))
                <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
                    @auth
                        <a href="{{ url('/dashboard') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">DASHBOARD</a>
                    @else
                        <a href="{{ route('login') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">LOGIN</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">REGISTER</a>
                           
                        @endif
                        <a href="{{ route('task') }}" class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">MY TASK</a>
                        <a href="{{ route('payment') }}" class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">PAYMENT</a>
                    @endauth
                </div>
            @endif
            <div class="max-w-6xl bg-white mx-auto sm:px-6 lg:px-8">
                <div class="flex justify-center pt-8 sm:justify-start sm:pt-0">
                    <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                        TASK 1 <br>
                        <?php 

                            function searchClosest($search, $array) { 
                                $number = 0;                            
                                if($search=="4.5"){
                                    $datanum = $search;
                                }else{
                                    $datanum = round($search);
                                }

                                foreach ($array as $data) {
                                if ($number === null || ($datanum - $number) > ($data - $datanum)){
                                        $number = $data;
                                    }
                                }
                                return "Answer : ".$number."<br>";
                            };

                            printf(searchClosest(4.5, array(-1.5, 0, 4.4, 5, 6, 7)));
                            printf(searchClosest(5.5, array(-1.5, 0, 4.4, 5, 6, 7)));

                            echo "<br><br>";

                            echo "TASK 2 <br>";
                            
                            function getTLD($level, $domain) {
                                $domain_pieces = explode(".", parse_url($domain, PHP_URL_HOST));
                                if($level=="2"){
                                    $find = $domain_pieces[$level];
                                        echo $find;
                                }
                                if($level=="3"){
                                    for($x=2; $x<=$level; $x++){
                                        if($x==3){
                                            echo ".";
                                        }
                                        echo $domain_pieces[$x];
                                    }
                                }
                            }

                            echo "Answer : "; 
                            printf(getTLD('2','http://www.google.de/something'));
                            echo "<br>";
                            echo "Answer : "; 
                            printf(getTLD('3','http://www.amazon.co.uk/something'));
                            echo "<br>";

                            /*

                            $urlData = "https://onfido.com/";
                            $captureImg=curl_init($urlData);                        
                            curl_setopt_array($captureImg,array(CURLOPT_ENCODING=>'',CURLOPT_RETURNTRANSFER=>1));
                            $htmlPage=curl_exec($captureImg);
                            preg_match_all('|<img.*?src=[\'"](.*?)[\'"].*?>|i', $htmlPage, $matches);
                            $loadData=@DOMDocument::loadHTML($htmlPage);
                            foreach($loadData ->getElementsByTagName("img") as $img){
                                $imgUrl=$img->getAttribute("src"); 
                                $imageGet = get_headers($imgUrl, 1);
                                $imgSize = $imageGet ["Content-Length"];

                                echo $imgUrl."-".$imgSize."<br>";
                            }
                            */
                            /*
                            class SudokuSolution {
                                function solveSudoku($board) {
                                    $gridSize = 9;
                                    $this->iterateGrid($board, 0, 0, $gridSize);
                                }

                                function iterateGrid($board, $row, $col, $gridSize){
                                    // check if reach end of array 8th row and 9th column. Stop iterating array.
                                    // check if reach row end 9th column. Increase the row and make column index 0
                                    if( $col == $gridSize ){
                                        $row++; $col = 0; }
                                    // check if column have digit already
                                    if($board[$row][$col] != '.')
                                    return $this->iterateGrid($board, $row, $col+1, $gridSize );
                                    for($num = 1; $num < 10; $num++){
                                        // check if number is valid for board box
                                        if($this->checkBoxNumber($board, $row, $col, $num)){
                                            // place value in current row and column
                                            $board[$row][$col] = (string) $num;
                                            // move to next column
                                            if($this->iterateGrid($board, $row, $col+1, $gridSize ))
                                            return true;
                                        }
                                        // make current column value empty
                                        $board[$row][$col] = '.';
                                    }
                                    return false;
                                }

                                function checkBoxNumber($board, $row, $col, $num){
                                // check same number in similar row
                                for($counter = 0; $counter < 9; $counter++)
                                    if($board[$row][$counter] == $num)
                                        return false;
                                // check same number in similar col
                                for($counter = 0; $counter < 9; $counter++)
                                    if($board[$counter][$col] == $num)
                                        return false;
                                // check the 3X3 sub box grid
                                $startRow = $row - $row % 3;
                                $startCol = $col - $col % 3;
                                for($rowCounter = 0; $rowCounter < 3; $rowCounter++)
                                    for($colCounter = 0; $colCounter < 3; $colCounter++)
                                    return true;
                                }
                            }

                            $board = [
                                ["5","3",".",".","7",".",".",".","."],
                                ["6",".",".","1","9","5",".",".","."],
                                [".","9","8",".",".",".",".","6","."],
                                ["8",".",".",".","6",".",".",".","3"],
                                ["4",".",".","8",".","3",".",".","1"],
                                ["7",".",".",".","2",".",".",".","6"],
                                [".","6",".",".",".",".","2","8","."],
                                [".",".",".","4","1","9",".",".","5"],
                                [".",".",".",".","8",".",".","7","9"]];

                            echo '<h2 align="center">Sudoku Box Unsolved</h2>
                            <table border="1" style="width:200px;" align="center">';
                            for($row = 0; $row < 9; $row++){
                                echo '<tr>';
                                for($col =0; $col <9; $col++){
                                    echo '<td align="center">'.$board[$row][$col].'</td>';
                                }
                                echo '</tr>';
                            }
                            echo '</table>';

                            $obj = new SudokuSolution;
                            $obj->solveSudoku($board);
                            echo '<h2 align="center">Sudoku Box Solved</h2>
                            <table border="1" style="width:200px;" align="center">';
                            for($row = 0; $row < 9; $row++){
                                echo '<tr>';
                                for($col =0; $col <9; $col++){
                                    echo '<td align="center">'.$board[$row][$col].'</td>';
                                }
                                echo '</tr>';
                            }
                            echo '</table>';
                        */

                        function recu($a, $b){
                            $dnum = ($a - $b) / 2;
                            if (is_int($dnum)) {
                                $c = ($a - $b) / 2;
                                return $c;
                            } else {   
                                $c = (($a - $b) - 1) / 2;
                                return $c.",".$c+1;
                            }
                        } 
                        
                        echo recu(1071,1029);
                    ?>
                    </div>
                </div>
            </div>
        </div>
       
    </body>
</html>
